import React from 'react'
import './Demo.scss'
import { toast } from 'react-toastify';

class ChildComponent extends React.Component {

    state = {
        showJobs: false
    }

    handleShowHide = () => {
        this.setState({
            showJobs: !this.state.showJobs
        })
    }

    handleOnClickDelete = (job) => {
        console.log('>>>> Handle on click Delete', job)
        this.props.deleteAJob(job)
        toast.success('Successfully Deleted')
    }
    render() {
        // console.log('>>>>Check Props:', this.props)
        // let name = this.props.name
        // let age = this.props.age

        let { arrJobs } = this.props
        let { showJobs } = this.state
        let check = showJobs === true ? 'showJobs = true' : 'showJobs = false'
        console.log('>>>Check condition: ', check)

        return (
            <>
                {showJobs === false ?
                    <div>
                        <button className='btn-show' onClick={() => this.handleShowHide()}> Show </button>
                    </div>
                    :
                    <>
                        <div>
                            <button onClick={() => this.handleShowHide()}> Hide </button>
                        </div>
                        <div className='job-lists'>
                            {
                                arrJobs.map((item, index) => {
                                    return (
                                        <div key={item.id}>
                                            {item.title} - {item.salary}
                                            <span onClick={() => this.handleOnClickDelete(item)}> x </span>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </>

                }
            </>
        )
    }
}

// const ChildComponent = (props) => {

//     console.log('>>>Check child props: ', props)

//     let { name, age, address, arrJobs } = props
//     return (
//         <React.Fragment>
//             <div> Child Component: {name}, Age: {age}, Address: {address}</div>
//             <div className='job-lists'>
//                 {
//                     arrJobs.map((item, index) => {
//                         if (item.salary >= 5000) {
//                             return (
//                                 <div key={item.id}>
//                                     {item.title} - {item.salary} $
//                                 </div>
//                             )
//                         }

//                     })
//                 }
//             </div>
//         </React.Fragment>
//     )
// }
export default ChildComponent
