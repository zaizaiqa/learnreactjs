import React from 'react'
import ChildComponent from './childComponent'
import AddComponent from './AddComponent'
class MyComponent extends React.Component {

    state = {
        // firstName: '',
        // lastName: '',
        arrJobs: [
            {
                id: 'Jobs1',
                title: 'Developer',
                salary: '5000'
            },
            {
                id: 'Jobs2',
                title: 'Tester',
                salary: '4000'
            },
            {
                id: 'Jobs3',
                title: 'Project Manager',
                salary: '6000'
            }
        ]
    }
    /*
    JSX syntax
    Fragment  <React.Fragment>
    */
    addNewJob = (job) => {
        //console.log('Check job from Parent:', job)
        this.setState({
            arrJobs: [...this.state.arrJobs, job]
        })

        // let currentJobs = this.state.arrJobs
        // currentJobs.push(job)
        // this.state({
        //     arrJobs: currentJobs
        // })

    }

    deleteAJob = (job) => {
        let currentJobs = this.state.arrJobs
        currentJobs = currentJobs.filter(item => item.id !== job.id)
        this.setState({
            arrJobs: currentJobs
        })
    }

    render() {
        console.log('>>>>Call render:', this.state)
        return (

            <React.Fragment>
                <AddComponent addNewJob={this.addNewJob}
                />

                <ChildComponent
                    // name={this.state.firstName}
                    // age={'30'}
                    // address={'Bac Ninh'}
                    arrJobs={this.state.arrJobs}
                    deleteAJob={this.deleteAJob}
                />
            </React.Fragment>


        )
    }
}

export default MyComponent