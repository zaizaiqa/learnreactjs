import React from "react";
import { withRouter } from "react-router-dom";
import Color from "../HOC/Color";
import logo from '../../assets/images/anh.jpg'
import { connect } from "react-redux";
import { toast } from "react-toastify";
class Home extends React.Component {

    componentDidMount() {
        setTimeout(() => {
            // this.props.history.push('/todos')
        }, 3000)
    }

    handleDeleteUser = (user) => {

        console.log('>>>Check delete users', user)
        this.props.deleteUserRedux(user)
        toast.success('Successfully Deleted')
    }

    handleCreateUser = () => {
        this.props.addUserRedux()
        toast.success('User Created')
    }

    render() {
        console.log('>>> Check props:', this.props.dataRedux)
        let listUsers = this.props.dataRedux

        return (
            <>
                <div> Hello World From HomePage</div>
                <div><img src={logo} style={{ width: '200px' }} /></div>
                <div>
                    {listUsers && listUsers.length > 0 &&
                        listUsers.map((item, index) => {
                            return (
                                <div key={item.id}>
                                    {index + 1} - {item.name}
                                    &nbsp; <span onClick={() => this.handleDeleteUser(item)}> x </span>
                                </div>
                            )
                        })
                    }
                    <button onClick={() => this.handleCreateUser()}>Add New</button>
                </div>
            </>

        )
    }
}
//export default withRouter(Home)
const mapStateProps = (state) => {
    return {
        dataRedux: state.users
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        deleteUserRedux: (userDelete) => dispatch({ type: 'DELETE_USER', payload: userDelete }),
        addUserRedux: () => dispatch({ type: 'CREATE_USER' })
    }
}
export default connect(mapStateProps, mapDispatchToProps)(Color(Home))
// export default Color(Home)