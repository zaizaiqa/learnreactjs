import React from "react";
import axios from "axios";
import './ListUser.scss'
import { toast } from "react-toastify";
import { withRouter } from "react-router-dom";

class ListUser extends React.Component {

    state = {
        listUser: []
    }

    async componentDidMount() {
        // axios.get('https://reqres.in/api/users?page=2')
        //     .then(res => {
        //         console.log('>>>Check res: ', res.data.data)
        //     })

        let res = await axios.get('https://reqres.in/api/users?page=1')
        this.setState({
            listUsers: res && res.data && res.data.data ? res.data.data : []
        })
    }

    handleViewUser = (user) => {

        this.props.history.push(`/users/${user.id}`)
        toast.success('View Successful')
    }

    render() {
        let { listUsers } = this.state
        return (
            <div className='list-user-container'>
                <div className='title'>
                    Fetch all list users
                </div>
                <div className='list-user-content'>
                    {listUsers && listUsers.length > 0 &&
                        listUsers.map((user, index) => {
                            return (
                                <div className='child' key={user.id} onClick={() => this.handleViewUser(user)}>
                                    {index + 1} - {user.first_name} {user.last_name}
                                </div>
                            )
                        })
                    }

                </div>
            </div>
        )
    }
}

export default withRouter(ListUser)