import React from "react"
import AddToDo from "./AddToDo"
import './ListToDo.scss'
import { toast } from 'react-toastify';
import Color from "../HOC/Color";

class ListToDo extends React.Component {

    state = {
        listToDos: [
            { id: 'todo1', title: 'Doing homework' },
            { id: 'todo2', title: 'Making video' },
            { id: 'todo3', title: 'Fixing bugs' }
        ],
        editTodo: {}
    }

    addNewToDo = (todo) => {
        this.setState({
            listToDos: [...this.state.listToDos, todo]
        })

        toast.success('Successfully Added.')

    }

    handleDeleteTodo = (todo) => {
        let { editTodo } = this.state
        let currentToDo = this.state.listToDos
        currentToDo = currentToDo.filter(item => item.id !== todo.id)

        this.setState({
            listToDos: currentToDo
        })
        toast.success('Successfully Deleted.')
    }
    handleEditToDo = (todo) => {
        let { editTodo, listToDos } = this.state
        let isEmptyObj = Object.keys(editTodo).length === 0
        if (isEmptyObj === false && editTodo.id === todo.id) {

            let listToDosCopy = [...listToDos]

            let objIndex = listToDosCopy.findIndex((item => item.id === todo.id));
            listToDosCopy[objIndex].title = editTodo.title

            this.setState({
                listToDos: listToDosCopy,
                editTodo: {}
            })
            toast.success('Successfully Updated.')
            return
        }

        this.setState({
            editTodo: todo
        })


    }

    handleOnChangeEditTodo = (event) => {
        let editTodoCopy = { ...this.state.editTodo }
        editTodoCopy.title = event.target.value
        this.setState({
            editTodo: editTodoCopy
        })
    }
    render() {
        let { listToDos, editTodo } = this.state
        let isEmptyObj = Object.keys(editTodo).length === 0
        console.log('>>>Check Empty Object: ', isEmptyObj)
        return (
            <>
                <p>
                    Simple ToDo Apps with React.JS
                </p>

                <div className='list-todo-container'>
                    <AddToDo
                        addNewToDo={this.addNewToDo}
                    />
                    <div className='list-todo-content'>
                        {listToDos && listToDos.length > 0 &&
                            listToDos.map((item, index) => {
                                return (
                                    <div className='todo-child' key={item.id}>
                                        {isEmptyObj === true ?
                                            <span> {index + 1} - {item.title} </span>
                                            :
                                            <>
                                                {editTodo.id === item.id ?
                                                    <span>
                                                        {index + 1} - <input value={editTodo.title} onChange={(event) => this.handleOnChangeEditTodo(event)} />
                                                    </span>
                                                    :
                                                    <span>
                                                        {index + 1} - {item.title}
                                                    </span>
                                                }

                                            </>

                                        }
                                        <button type='button' className='edit' onClick={() => { this.handleEditToDo(item) }}>
                                            {isEmptyObj === false && editTodo.id === item.id ? 'Save' : 'Edit'}
                                        </button>
                                        <button type='button' className='delete' onClick={() => this.handleDeleteTodo(item)}> Delete </button>
                                    </div>
                                )
                            })
                        }

                    </div>
                </div>

            </>

        )

    }
}

export default Color(ListToDo)
