import React from "react";
import './Nav.scss'
import {
    Link, NavLink
} from "react-router-dom"


class Nav extends React.Component {

    render() {
        return (
            <div className="topnav">
                {/* <Link to="/">Home</Link>
                <Link to="/todos">Todos</Link>
                <Link to="/jobs">Jobs</Link> */}

                <NavLink to="/" activeClassName="active" exact>Home</NavLink>
                <NavLink to="/todos" activeClassName="active">Todos</NavLink>
                <NavLink to="/jobs" activeClassName="active">Jobs</NavLink>
                <NavLink to="/users" activeClassName="active">Users</NavLink>

                {/* <a className="active" href="/">Home</a>
                <a href="/todos">Todos</a>
                <a href="/jobs">Jobs</a>
                <a href="/users">Users</a> */}
            </div>
        )
    }
}
export default Nav